import React from 'react'
import SofashieldLogo from '../images/sofashield-logo.png'
import Layout from '../components/layout'
import '../styles/sofashield.css'
import SofashieldExample from '../images/sofashield-example.jpg'
function sofashield() {
    return (
        <Layout>
            <div className="sofashield-top-container">
                <h1 className ="sofashield-title">Live life the way you want to</h1>
                <p className ="sofashield-intro">A spill or a scratch on your dream sofa can be hard to live with. We all eat and drink on our sofas - relaxing<br /> with some snacks or a glass of wine is all part of enjoying our time at home, cosied up on the sofa with our<br /> families. You shouldn’t have to feel like you can’t relax the way you want to, or live in constant fear of staining<br /> your sofa. And if you have children or pets, that’s even more to worry about.</p>
                <img src={SofashieldLogo} alt="" />
            </div>
            <div className="hero-image-container">
            <img src={SofashieldExample} alt=""/>
                <div className="hero-image-text-container">
                    <h2 className="hero-image-title">Protect your sofa with Sofashield</h2>
                    <p className="hero-image-text">Spill your coffee or accidentally rip your sofa and that <br /> stain or tear will catch your eye every time you curl up to <br /> watch TV. But with the Sofashield professionals just a<br /> phone call away, you can rest assured that you’ve got fast,<br /> effective help at your fingertips.</p>

                </div>

            </div>

            <div className="sofashield-filler-info">
                <h2 className="sofashield-filler-title-one">Sofashield gives you professional assistance for up to 5 years, protecting your <br /> sofa against accidental stains, scratches and damage up to the full value of <br /> your sofa.</h2>
                <p className="sofashield-filler-content">Call out the Sofashield professionals within 72 hours of the accident happening and they’ll be there to take <br /> care of the damage. If they can’t repair it, they’ll replace the part - or even the whole sofa if they have to. This <br /> is not a cleaning service, so everyday dirt or wear and tear isn’t part of the deal. Unfortunately, Sofashield <br /> was created for Sofology sofas only, so you can’t use it on a sofa you already own. It can be added to your <br /> Sofology sofa order right up until delivery day - but after that you’re on your own.</p>
                <h2 className="sofashield-filler-title-two">Peace of mind for 10% of the value of your furniture</h2>
                <p className="sofashield-filler-content">Whether your sofa is fabric or leather the price is the same. Whether your sofa is fabric or leather, the price of <br /> Sofashield is always 10% of the value of your furniture. So, if your sofa cost £899, you can relax with <br /> complete peace of mind for £89.90</p>
            </div>
        </Layout>
    )
}

export default sofashield
