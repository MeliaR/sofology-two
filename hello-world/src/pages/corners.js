import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/layout'
import Logo from '../images/logo2.png'
import Colors from '../images/colors.png'
import Sizes from '../images/sizeicon2.png'
import Palm from '../images/palm-corner.jpg'
import '../styles/rangepage.css'

const corners = (props) => {
    const sofas = props.data.allMongodbSofologyCorners.edges;
    return (
        <Layout>
            <img className="Banner" alt="palmsofa" src={Palm} />
            <div className="column">
                {sofas.map(sofa =>
                    <div className="card">
                        <div className="rangeName">
                            <img alt="logo" className="logo" src={Logo} />
                            <h1><b>{sofa.node.rangeName} Range</b></h1>
                        </div>
                        <img className="heroImg" alt="sofas" src={sofa.node.cdnHeroImage} />
                        <div className="colorVariant">
                            <img className="colors" alt="colors" src={Colors} />
                            <p>{sofa.node.numberOfVariants} Colours</p>
                        </div>
                        <div className="sizeVariant">
                            <img className="sizes" alt="size" src={Sizes} />
                            <p>{sofa.node.numberOfProductItems} Sizes</p>
                        </div>
                    </div>
                )}
            </div>
        </Layout>
    );
}

export default corners;

export const PageQuery = graphql`
query{
    allMongodbSofologyCorners(limit:9) {
      edges {
        node {
          cdnHeroImage
          rangeName
          webDescription
          currentPrice
          monthlyPrice
          numberOfVariants
          numberOfProductItems
          numberOfLikes
        }
      }
    }
  }`
