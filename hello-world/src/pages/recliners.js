import { graphql } from 'gatsby';
import React from 'react';
import Layout from '../components/layout'
import Logo from '../images/logo2.png'
import '../styles/rangepage.css'
import Colors from '../images/colors.png'
import Sizes from '../images/sizeicon2.png'

const recliners = (props) => {
    const sofas = props.data.allMongodbSofologyRecliners.edges;
    return (
        <Layout>
      <div className="column">
        {sofas.map(sofa =>
          <div className="card">
            <div className="rangeName">
              <img alt="logo" className="logo" src={Logo} />
              <h1><b>{sofa.node.rangeName} Range</b></h1>
            </div>
            <img className="heroImg" alt="sofas" src={sofa.node.cdnHeroImage} />
            <div className="colorVariant">
              <img className="colors" alt="colors" src={Colors} />
              <p>{sofa.node.numberOfVariants} Colours</p>
            </div>
            <div className="sizeVariant">
              <img className="sizes" alt="size" src={Sizes} />
              <p>{sofa.node.numberOfProductItems} Sizes</p>
            </div>
          </div>
        )}
      </div>
        </Layout>
    );
}

export default recliners;

export const PageQuery = graphql`
query{
    allMongodbSofologyRecliners(limit:9) {
      edges {
        node {
          cdnHeroImage
          rangeName
          webDescription
          currentPrice
          numberOfVariants
          numberOfProductItems
        }
      }
    }
  }`
