import React, { useState, useEffect } from 'react'
import sanityClient from '../client';
import { Link } from 'gatsby'
import imageUrlBuilder from '@sanity/image-url'
import Layout from '../components/layout'
import './cushions.css'
import CushionLogo from '../images/cushions-title-logo.png'
import CushionHero from '../images/cushions-hero.jpg'


function Cushions() {
    const [product, setProduct] = useState(null);
    const query = `*[_type == "product"]{title, body,slug, dimensions, delivery,image{asset->},list,price}`
    const builder = imageUrlBuilder(sanityClient)

    function urlFor(source) {
        return builder.image(source)
    }

    useEffect(() => {
        sanityClient.fetch(query)
            .then((data) => {
                setProduct(data)
                console.log(data)
            }

            )
    }, []);
    return (
        <Layout>
            <div className="banner-header-container">
                <img className="banner-hero-image" src={CushionHero} alt=""/>
            </div>    
            <div className="cushion-hero-content">    
                <img className="banner-cushion-title" src={CushionLogo} alt="" />
                <p className="banner-filler-text">Mix and match scatter cushions to give your sofa a<br /> whole new style. From a simple pop of colour to<br /> contrasting patterns, the right bolster or scatter<br /> cushion will transform your whole living room.</p>
            </div>
            <div className="product-main-container">

                {product && product.map((product, index) => (
                    <Link to={"/cushions/" + product.slug.current} key={product.slug.current}>
                        <span className="product-item-container" key={index}>
                            <img className="product-image" src={urlFor(product.image).url()} />
                            <h1 className="product-title">{product.title}</h1>
                            <h2 className="product-price">{product.price}</h2>
                        </span>
                    </Link>
                ))}
            </div>
        </Layout>
    )
}

export default Cushions
