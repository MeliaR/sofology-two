import React from "react"
import Layout from "../components/layout"
import 'react-slideshow-image/dist/styles.css'
import Slideshow from "../components/slidelanding"
import ShopSafely from "../components/shopSafely"
import ShopSofas from "../components/shopSofas"

function Home() {
  return (
    <Layout>
      <Slideshow />
      <ShopSafely />
      <ShopSofas />
    </Layout>
  )
}

export default Home
