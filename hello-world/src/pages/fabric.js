import React from 'react';
import Layout from '../components/layout'
import '../styles/rangepage.css'
import { graphql } from 'gatsby'
import Logo from '../images/logo2.png'
import Colors from '../images/colors.png'
import Sizes from '../images/sizeicon2.png'
import fabBanner from '../images/fabric-desktop.jpg'

const fabric = (props) => {
  const sofas = props.data.allMongodbSofologyRanges.edges;
  return (
    <Layout>
      <img className="Banner" alt="banner" src={fabBanner}/>
      <h1 className="showing">Showing <b>1-9</b> of 178 in <b>Fabric Sofas</b></h1>
      <div className="column">
        {sofas.map(sofa =>
          <div className="card">
            <div className="rangeName">
              <img alt="logo" className="logo" src={Logo} />
              <h1><b>{sofa.node.rangeName} Range</b></h1>
            </div>
            <img className="heroImg" alt="sofas" src={sofa.node.cdnHeroImage} />
            <div className="colorVariant">
              <img className="colors" alt="colors" src={Colors} />
              <p>{sofa.node.numberOfVariants} Colours</p>
            </div>
            <div className="sizeVariant">
              <img className="sizes" alt="size" src={Sizes} />
              <p>{sofa.node.numberOfProductItems} Sizes</p>
            </div>
          </div>
        )}
    </div>
</Layout>
  );
}

export default fabric;

export const PageQuery = graphql`
query {
    allMongodbSofologyRanges(limit:9) {
      edges {
        node {
          cdnHeroImage
          rangeName
          webDescription
          currentPrice
          numberOfVariants
          numberOfProductItems
        }
      }
    }
  }
`
