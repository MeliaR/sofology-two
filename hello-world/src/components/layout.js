import React from 'react'
import Header from './header'
import Footer from './footer'
import Navbar from './navbar'
import Trustpilot from './trustpilot'

function Layout(props) {
    return (
        <div>
            <Trustpilot />
            <Header />
            <Navbar />
            {props.children}
            <Footer />

        </div>
    )
}

export default Layout
