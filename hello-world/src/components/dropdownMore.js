import React from 'react'
import { Link } from 'gatsby'
import './dropdownMenu.css'

function DropdownMore() {


    return (
        <div className="dropdown">

            <Link className="menu-item" to='/responsibility/order-direct'>Order direct & Advice</Link><br />
            <Link className="menu-item" to='/responsibility/what-is-Sofology'>What is Sofology?</Link><br />
            <Link className="menu-item" to='/responsibility/product'>Delivery</Link><br />
            <Link className="menu-item" to='/responsibility/people'>Payment Options</Link><br />
            <Link className="menu-item" to='/sofashield'>SofaShield</Link><br />
            <Link className="menu-item" to='/responsibility/sofaglide'>Sofaglides</Link><br />
            

        </div>
    )
}

export default DropdownMore
