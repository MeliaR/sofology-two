import React from 'react'
import { Link } from 'gatsby'
import './dropdownMenu.css'



function DropdownSofas() {


    return (
        <div className="dropdown">

            <Link className="menu-item" to='/cushions'>Cushions</Link><br />
            <Link className="menu-item" to='/rugs'>Rugs</Link><br />
            <Link className="menu-item" to='/tables'>Tables</Link><br />
            <Link className="menu-item" to='/lighting'>Lighting</Link><br />


        </div>
    )
}

export default DropdownSofas
