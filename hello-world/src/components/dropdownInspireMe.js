import React from 'react'
import { Link } from 'gatsby'
import './dropdownMenu.css'


function DropdownInspireMe() {


    return (
        <div className="dropdown">

            <Link className="menu-item" to='/sofological-blog'>Sofological Blog</Link><br />
            <Link className="menu-item" to='/our-sofas-feeling-at-home'>Our Sofas in your homes</Link><br />
            <Link className="menu-item" to='/shop-the-room'>Shop The Room</Link><br />
            <Link className="menu-item" to='/industrial'>Buying Guides</Link><br />
            
        </div>
    )
}

export default DropdownInspireMe
