import React from 'react'
import LandingFabric from "../images/fabric-landing.png"
import LandingLeather from "../images/leather-landing.png"
import CornerLogo from '../images/corner-logo.jpg'
import SofaBedLogo from '../images/sofa-bed-logo.jpg'
import ReclinerLogo from '../images/recliner-logo.jpg'
import AccentLogo from '../images/accent-chair-logo.jpg'
import GreyColor from '../images/grey-color.png'
import PinkColor from '../images/pink-color.png'
import GreenColor from '../images/green-color.png'
import BlueColor from '../images/blue-color.png'
import OrangeColor from '../images/orange-color.png'
import CreamColor from '../images/cream-color.png'
import RedColor from '../images/red-color.png'
import BrownColor from '../images/brown-color.png'
import FourSeater from '../images/four-seater.png'
import ThreeSeater from '../images/three-seater.png'
import TwoSeater from '../images/two-seater.png'
import Chair from '../images/chairs.png'
import Footstools from '../images/footstools.png'
import './shopSofas.css'

import { Link } from 'gatsby'

function ShopSofas() {
    return (
        <div>
            <h2 className="shop-sofas-title">Shop sofas</h2>
            <div className="shop-the-sofa-container">
                <div className="shop-sofa-landing-fabric">
                    <Link to="/fabric">
                        <img className="landing-fabric-sofas" src={LandingFabric} alt="" />
                        <p className="shop-sofas-p">Fabric Sofas</p>
                    </Link>
                </div>
                <div className="shop-sofa-landing-leather">
                    <Link to="/leather">
                        <img className="landing-leather-sofas" src={LandingLeather} alt="" />
                        <p className="shop-sofas-p">Leather Sofas</p>
                    </Link>
                </div>
            </div>
            <div className="other-types-container">
                <div className="other-types-one-container">
                    <Link to="/corners">
                        <img className="other-types-corner" src={CornerLogo} />
                        <p className="other-types-corner-p">Corner Sofas</p>
                    </Link>
                    <Link to="/sofabed">
                        <img className="other-types-sofabed" src={SofaBedLogo} />
                        <p className="other-types-sofabed-p">Sofa Beds</p>
                    </Link>
                </div>
                <div className="other-types-two-container">
                    <Link to="/accent">
                        <img className="other-types-accent" src={AccentLogo} />
                        <p className="other-types-accent-p">Accent Chairs</p>
                    </Link>
                    <Link to="/recliner">
                        <img className="other-types-recliner" src={ReclinerLogo} />
                        <p className="other-types-sofabed-p">Recliners</p>
                    </Link>
                </div>
            </div>
            <div className="shop-by-color-container">
                <h2 className="sofa-colors-title">Shop by colour</h2>
                <div className="sofa-colors">
                    <div>
                        <img className="color-image" src={GreyColor} alt="" />
                        <p>Grey</p>
                    </div>
                    <div>
                        <img className="color-image" src={PinkColor} alt="" />
                        <p>Pink</p>
                    </div>
                    <div>
                        <img className="color-image" src={GreenColor} alt="" />
                        <p>Green</p>
                    </div>
                    <div>
                        <img className="color-image" src={BlueColor} alt="" />
                        <p>Blue</p>
                    </div>
                    <div>
                        <img className="color-image" src={OrangeColor} alt="" />
                        <p>Orange</p>
                    </div>
                    <div>
                        <img className="color-image" src={CreamColor} alt="" />
                        <p>Cream</p>
                    </div>
                    <div>
                        <img className="color-image" src={RedColor} alt="" />
                        <p>Red</p>
                    </div>
                    <div>
                        <img className="color-image" src={BrownColor} alt="" />
                        <p>Brown</p>
                    </div>
                </div>
            </div>
            <div className="shop-by-size-title">
                <h2>Shop by size</h2>
                <div className="shop-by-size-container">
                    <div className="size-container">
                        <img className="four-seaters" src={FourSeater} alt="" />
                        <p>Four Seaters</p>
                    </div>
                    <div className="size-container">
                        <img className="three-seaters" src={ThreeSeater} alt="" />
                        <p>Three Seaters</p>
                    </div>
                    <div className="size-container">
                        <img className="two-seaters" src={TwoSeater} alt="" />
                        <p>Two Seaters</p>
                    </div>
                    <div className="size-container">
                        <img className="chairs" src={Chair} alt="" />
                        <p>Chairs</p>
                    </div>
                    <div className="size-container">
                        <img className="footstools" src={Footstools} alt="" />
                        <p>Footstools</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ShopSofas
