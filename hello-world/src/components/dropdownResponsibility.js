import React from 'react'
import { Link } from 'gatsby'
import './dropdownMenu.css'

function DropdownResponsibility() {


    return (
        <div className="dropdown">

            <Link className="menu-item" to='/responsibility/environment'>Environment</Link><br />   
            <Link className="menu-item" to='/responsibility/product'>Product</Link><br />
            <Link className="menu-item" to='/responsibility/people'>People</Link><br />
            <Link className="menu-item" to='/responsibility/charity'>Charity</Link><br />

        </div>
    )
}

export default DropdownResponsibility
