import React from 'react'
import { Link } from 'gatsby'
import './dropdownMenu.css'


function DropdownSofas() {


    return (
        <div className="dropdown">

            <Link className="menu-item" to='/fabric'>Fabric Sofas</Link><br />
            <Link className="menu-item" to='/leather'>Leather Sofas</Link><br />
            <Link className="menu-item" to='/corner'>Corner Sofas</Link><br />
            <Link className="menu-item" to='/recliner'>Recliner Sofas</Link><br />
            <Link className="menu-item" to='/sofabeds'>Sofa Beds</Link><br />
            <Link className="menu-item" to='/chairs'>Chairs</Link><br />
            <Link className="menu-item" to='/newin'>New In</Link><br />
            <Link className="menu-item" to='/sofasizer'>Sofasizer</Link><br />

        </div>
    )
}

export default DropdownSofas
