import React from 'react'
import { Link } from 'gatsby'
import FabricIcon from '../images/fabric-icon.png'
import LeatherIcon from '../images/leather-icon.png'
import CornerIcon from '../images/corners-icon.png'
import ReclinerIcon from '../images/recliners-icon.png'
import SofabedIcon from '../images/sofa-bed-icon.png'
import Sofasizer from '../images/sofasizer.png'
import FindAStore from '../images/find-a-store.png'
import OrderDirect from '../images/order-direct.png'
import CustomerServices from '../images/customer-service.png'
import ChairIcon from '../images/chairs-icon.png'
import './navbar.css'

function Navbar() {
    return (
        <div className ="navbar-container">
            <nav>
                <ul className="navbar-link-list">
                    <li>
                        <Link className="nav-link" to="/fabric">
                        <img className="nav-fab-icon" src={FabricIcon} alt="" />
                        Fabric</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/leather">
                        <img className="nav-leather-icon" src={LeatherIcon} alt="" />
                        Leather</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/corners">
                        <img className="nav-corner-icon" src={CornerIcon} alt="" />
                        Corners</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/recliners">
                        <img className="nav-recliner-icon" src={ReclinerIcon} alt="" />
                        Recliners</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/sofabeds">
                        <img className="nav-sofabed-icon" src={SofabedIcon} alt="" />
                        Sofa Beds</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/chairs">
                        <img className="nav-chair-icon" src={ChairIcon} alt="" />
                        Chairs</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/sofasizer">
                        <img className="nav-sofasizer-icon" src={Sofasizer} alt="" />
                        Sofasizer</Link>
                    </li>
                    <li className="find-a-store-borderleft">
                        <Link className="nav-link" to="/findastore">
                        <img className="nav-find-a-store-icon" src={FindAStore} alt="" />
                        Find a store</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/orderdirect">
                        <img className="nav-order-direct-icon" src={OrderDirect} alt=""/>
                        Order Direct</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="/customerservices">
                        <img className="nav-customer-service" src={CustomerServices} alt=""/>
                        Customer Services</Link>
                    </li>

                </ul>
            </nav>
        </div>
    )
}

export default Navbar
