import React, { useState } from 'react'
import { Link } from 'gatsby'
import './navItem.css'

function NavItem(props) {

    const [open, setOpen] = useState(false);

    return (
        <li className="nav-item" onMouseEnter={() => setOpen(true)} onMouseLeave={() => setOpen(false)}>
            <Link className="icon-button" to="#" >
                {props.icon}
            </Link>

            {open && props.children}
        </li>

    )
}

export default NavItem;
