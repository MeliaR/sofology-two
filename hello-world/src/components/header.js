import React from 'react'
import DropdownSofas from './dropdownSofas'
import DropdownAccessories from './dropdownAccessories'
import DropdownInspireMe from './dropdownInspireMe'
import DropdownResponsibility from './dropdownResponsibility'
import DropdownMore from './dropdownMore'
import HeaderNav from './headerNav'
import NavItem from './navItem'


function Header() {
    return (
        <HeaderNav>
            <NavItem icon="Sofas ⌄" >
                <DropdownSofas />
            </NavItem>
            <NavItem icon="Accessories ⌄" >
                <DropdownAccessories />
            </NavItem>
            <NavItem icon="Inspire Me ⌄" >
                <DropdownInspireMe />
            </NavItem>
            <NavItem icon="Find a Store" />
            <NavItem icon="Responsibility ⌄">
                <DropdownResponsibility />
            </NavItem>
            <NavItem icon="More ⌄" >
                <DropdownMore />
            </NavItem>
            <NavItem className="call-to-order" icon="Call to Order" />
            <NavItem icon="Clearance" />
        </HeaderNav>
    );
}

export default Header