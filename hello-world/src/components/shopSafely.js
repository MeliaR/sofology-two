import React from 'react'
import { Link } from 'gatsby'
import './shopSafely.css'
import ShopSafe from '../images/shop-safe.png'
import SafeVideo from '../images/safe-video-call.png'
import SafeCall from '../images/safe-call-free.png'
import SafeChat from '../images/safe-web-chat.png'

function ShopSafely() {
    return (
        <div>
            <div className="shop-safely-info-container">
                <p><Link className="shop-safely-promise" to="/what-is-sofology"> No Sale Promise </Link></p>
                <p><Link className="shop-safely-interest-free" to="/payment-options">Interest Free 0% APR (Minimum Spend<br />£600)</Link></p>
                <p><Link className="shop-safely-expert-delivery" to="/delivery">Expert Delivery Included</Link></p>
                <p><Link className="shop-safely-guarentee" to="/guarentee">20 Year Structural Guarantee</Link></p>
            </div>
            <div className="shop-safe-banner-container">
                <img className="shop-safe" src={ShopSafe} alt="" />
                <div className="safe-video-container">
                    <img className="safe-video" src={SafeVideo} alt="" />
                    <p>Video Call a Sofologist</p>
                </div>
                <div className="safe-call-container">
                    <img className="safe-call" src={SafeCall} alt="" />
                    <p>Call Free on 0800 140 40 40</p>
                </div>
                <div className="safe-chat-container">
                    <img className="safe-chat" src={SafeChat} alt="" />
                    <p>Online & Webchat</p>
                </div>
            </div>
        </div>
    )
}

export default ShopSafely
