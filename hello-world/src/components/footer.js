import React from 'react'
import './footer.css'

function Footer() {
    return (
        <div className="title-footer">
            <h2 className="footer-title">Thanks for shopping with Sofology</h2>
        <div className="footer-container">
            <div className="first-column-list">
            <ul>
                <li>
                    Leather Sofas
                </li>
                <li>
                    Fabric Sofas
                </li>
                <li>
                    Corner Sofas
                </li>
                <li>
                    Recliner Sofas
                </li>
                <li>
                    Sofa Beds
                </li>
                <li>
                    Chairs
                </li>
            </ul>
            </div>
            <div className="middle-column-list">
            <ul>
                <li>
                    Contact us
                </li>
                <li>
                    Sofashield
                </li>
                <li>
                    Plant a tree
                </li>
                <li>
                    Careers
                </li>
                <li>
                    Customer Charter
                </li>
            </ul>

            </div>
        </div>
        </div>
    )
}

export default Footer
