import React from 'react';
import { Fade } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ComfortLogo from '../images/comfort-logo.png'
import ComfortIcons from '../images/comfort-icons.png'
import './slidelanding.css'

const fadeImages = [
    "https://images.sofology.co.uk/q_70,fl_lossy,f_auto/v1611919906/umbraco%20website/comfortfaves-homepage-desktop_wwheli.jpg.jpg",
    "https://images.sofology.co.uk/q_70,fl_lossy,f_auto/v1616411098/launch-desktop_02_gvzx0v.jpg",
    "https://images.sofology.co.uk/q_70,fl_lossy,f_auto/v1611668043/corner-sofas-desktop_cora_nuzyzc.jpg"
];

const Slideshow = () => {
  return (
    <div className="slide-container">
      <Fade>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[0]} />
          </div>
          <div className="comfort-content"> 
            <img className="comfort-logo-white" src={ComfortLogo} alt="" />
            <p className="comfort-filler-text">It can be hard to know if your dream<br />sofa is as comfy as it looks, so to give<br />you a helping hand we've put<br />together a collection of sofas that<br /> have been chosen as Comfort<br /> Favourites by our customers. </p>
            <img className="confort-icons" src={ComfortIcons} alt=""/>
            <button className="find-out-more-btn">FIND OUT MORE</button>
          </div> 
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[1]} />
          </div>
          <div className="special-buy-container">
            <h2 className="special-buy-title">Amazing comfort, <br /> amazing price</h2>
            <p className="special-buy-filler-text">Looking for something unique? Our<br /> Mimi special buy sofa is crafted from<br />exclusive, end of the line microfibre fabric<br /> in a stunning slate grey shade. <br />Delivered in just three weeks, there's<br />only a limited number of 2 and 3<br /> seater sofa sizes available. Book a<br /> Sofologist Live video call today to<br /> view the Mimi special buy in one of<br /> our stores.</p>
            <div className="special-buy-button-container">
                <button className="special-buy-shop-now">SHOP NOW</button>
                <button className="special-buy-video-call">VIDEO CALL</button>
            </div>
          </div>
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[2]} />
          </div>
          <div className="corner-sofas-container">
              <h2 className="corner-sofas-title">Room for one<br />more?</h2>
              <p className="corner-sofas-filler-text">Perfect for open plan spaces or cosy family living<br />rooms, a corner sofa offers extra space to stretch<br />out and relax. Thinking of choosing a corner sofa?<br />Check out our buying guide for more information on<br />measuring, positioning and the different styles<br />available.</p>
          <div className="corner-sofas-button-container">
              <button className="corner-sofas-shop">SHOP CORNER SOFAS</button>
              <button className="corner-sofas-buying-guide">CORNER SOFAS BUYING GUIDE</button>
          </div>
          
          </div>
        </div>
      </Fade>
    </div>
  )
}

export default Slideshow