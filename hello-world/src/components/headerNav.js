import React from 'react'
import './headerNav.css'
import SofologyLogo from '../images/sofology-logo.png'
import {Link} from 'gatsby'
function HeaderNav(props) {
    return (
        <div>
            <nav className='navbar'>
                <Link to="/">
                <img className="head-sofology-logo" src={SofologyLogo} alt=""/>
                </Link>
                <ul className='navbar-nav'>{props.children}</ul>
            </nav>
        </div>
    )
}

export default HeaderNav
