import React from 'react'
import TrustpilotStars from '../images/trustpilot-stars.png'
import TrustpilotLogo from '../images/trustpilot-logoname.png'
import Heart from '../images/heart.png'
import Cart from '../images/cart.png'
import './trustpilot.css'

function Trustpilot() {
    return (
        <div className="trustpilot-container">
            <h1 className="trustpilot-content">Excellent <img className="tpStars" src={TrustpilotStars} alt="" /> 132,000 reviews on <img className="TPLogo" src={TrustpilotLogo} alt="" /></h1>

            <div className="Faves">
                <div className="wrapContainer">
                <img className="heartImage" src={Heart} alt="" />
                <span>My Favourites |</span>
                <img className="cartImage" src={Cart} alt="" />
                <span> | Log in</span>
                </div>
            </div>

        </div>
    )
}

export default Trustpilot
