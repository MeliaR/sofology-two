export default {
  name: 'product',
  title: 'Product',
  type: 'document',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 60,
      }
    },
    {
      name: 'price',
      title: 'Price',
      type: 'string',
    },
    {
      name: 'dimensions',
      title: 'Dimensions',
      type: 'string',
    },
    {
      name: 'delivery',
      title: 'Delivery',
      type: 'string',
    },
    {
      name: 'body',
      title: 'Body',
      type: 'string',
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
    },
    {
      name: 'list',
      title: 'List',
      type: 'array',
      of: [{type: 'string'}]
    }
  ]}