/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {

  /* Your site config here */
  plugins: ['gatsby-plugin-postcss',
    {
      resolve: 'gatsby-source-mongodb',
      options: {
        dbName: 'Sofology',
        collection: ['Ranges', 'Leathers', 'Recliners', 'Sofabeds', 'Corners'],
        server: {
          address: 'cluster0-shard-00-02.cifmq.mongodb.net',
          port: 27017
        },
        auth: {
          user: 'bob-bobby',
          password: 'bobby69',
          user: 'jakeyd',
          password: 'jakeyd1'
        },
        extraParams: {
          replicaSet: 'cluster0-shard-0',
          ssl: true,
          authSource: 'admin',
          retryWrites: true
        },
        preserveObjectIds: true,
      }
    },
  ]
}
